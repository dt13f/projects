# CONTACT APP README

## Requirements:

  - Distributed version control with Git and Bitbucket
  - Development Installations
  - Bitbucket repos links

## Assignment screenshots:

*Java Installation:*


![Java install screenshot](img/java.PNG)

*Android Studio - My First App:*


![Hello World screenshot](img/android_hello.PNG)

*Contacts App - Main Screen:*


![Main screen screenshot](img/contact_main.PNG)

*Contacts App - Info Screen:*


![Info screen screenshot](img/contact_info.PNG)


## Tutorial Links:

[Bitbucket Station Locations](https://bitbucket.org/dt13f/bitbucketstationlocations)

[My Team Quotes](https://bitbucket.org/dt13f/myteamquotes)