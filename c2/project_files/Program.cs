﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace p2
{
    
	public class Program
	{
    	public static void Main()
    	{

    		string requirements = 
@"///////////////////////////////////////////////////////////////////////////
Program Requirements:
Using LINQ (Language Integrated Query)
Author: Dylan Thomas

*After* completing the required tutorial, create the following program:

1) Prompt user for last name. Return full name, occupation, and age

2) Prompt user for age and occupation (Dev or Manager). Return full name.
	(MUST include data validation on numeric data.)

3) Allow user to press any key to return back to command line.

*Notes*
 - LINQ uses programming language syntax to query data.
 - LINQ uses SQL-like syntax to produce usable objects.
 - LINQ can be used to query many different types of data 
 	including relational, XML, and even objects.

///////////////////////////////////////////////////////////////////////////";

			Console.WriteLine(requirements);

			Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));

    		var people = GenerateListOfPeople();

    		Console.WriteLine("\n***Finding Items in Collections*** ");

    		//list people over 30
    		Console.WriteLine("\nWhere: ");
        	
        	var peopleOver30 = people.Where(x => x.Age >30);
        	foreach (var person in peopleOver30)
        	{
        		Console.WriteLine(person.FirstName);
        	}

        	//skip erick and steve
        	Console.WriteLine("\nSkip: ");
			
        	IEnumerable<Person> afterTwo = people.Skip(2);
        	foreach (var person in afterTwo)
        	{
        		Console.WriteLine(person.FirstName);
        	}

        	//only list first two people (eric and steve)
        	Console.WriteLine("\nTake: ");
        	IEnumerable<Person> takeTwo = people.Take(2);
        	foreach (var person in takeTwo)
        	{
        		Console.WriteLine(person.FirstName);
        	}
        
        	Console.WriteLine("\n***Changing Each Item in Collections*** ");

        	//First names only
        	Console.WriteLine("\nFirst Names: ");
        	IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
        	foreach (var firstName in allFirstNames)
        	{
        		Console.WriteLine(firstName);
        	}

        	//Full names
        	Console.WriteLine("\nFull Names: ");
    	   	IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
    	   	foreach (var fullName in allFullNames)
    	   	{
    	   		Console.WriteLine($"{fullName.Last}, {fullName.First}");
    	   	}

    	   	Console.WriteLine("\n***Finding One Item in Collections*** ");

    	   	//first or default
    	   	Console.WriteLine("\nFirst or Default: ");
    	   	Person firstOrDefault = people.FirstOrDefault();
    	   	Console.WriteLine(firstOrDefault.FirstName);

    	   	//filter
    	   	Console.WriteLine("\nFirst or Default As Filter: ");
			var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
        	var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
        	Console.WriteLine(firstThirtyYearOld1.FirstName); 
        	Console.WriteLine(firstThirtyYearOld2.FirstName); 

        	Console.WriteLine("\nHow OrDefault: ");
        	List<Person> emptyList = new List<Person>();
			Person willBeNull = emptyList.FirstOrDefault(); 

			Person willAlsoBeNull = people.FirstOrDefault(x => x.FirstName == "John"); 

			Console.WriteLine(willBeNull == null);
			Console.WriteLine(willAlsoBeNull == null); 

			//Last or default
			Console.WriteLine("\nLastOrDefault: ");
			Person lastOrDefault = people.LastOrDefault();
       		Console.WriteLine(lastOrDefault.FirstName);
        	Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
        	Console.WriteLine(lastThirtyYearOld.FirstName);

        	//single or default
        	Console.WriteLine("\nSingleOrDefault: ");
        	Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); 
        	Console.WriteLine(single.FirstName);
        	// Person singleDev = people.SingleOrDefault(x => x.Occupation == "Dev");

        	Console.WriteLine("\n***Finding Data About Collections***");

        	//counts number of people in list
        	Console.WriteLine("\nCount: ");
        	int numberOfPeopleInList = people.Count();
        	Console.WriteLine(numberOfPeopleInList);

        	//count people over 25
        	Console.WriteLine("\nCount People Over 25: ");
        	int peopleOverTwentyFive = people.Count(x => x.Age > 25);
        	Console.WriteLine(peopleOverTwentyFive);

        	//any (bool values)
			Console.WriteLine("\nAny: ");
        	bool thereArePeople = people.Any();
        	Console.WriteLine(thereArePeople);
        	bool thereAreNoPeople = emptyList.Any();
        	Console.WriteLine(thereAreNoPeople);

        	//all
        	Console.WriteLine("\nAll: ");
        	bool allDevs = people.All(x => x.Occupation == "Dev");
        	Console.WriteLine(allDevs);
        	bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
        	Console.WriteLine(everyoneAtLeastTwentyFour);

        	Console.WriteLine("\n***Converting Results to Collections***");

        	//tolist
        	Console.WriteLine("\nToList: ");
        	List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList();
        	
        	foreach (var person in listOfDevs)
        	{
        		Console.WriteLine(person.FirstName);
        	}


        	//toarray (returns same names as list)
        	Console.WriteLine("\nToArray: ");
        	Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray(); //This will return a Person[] array
        	foreach (var person in arrayOfDevs)
        	{
        		Console.WriteLine(person.FirstName);
        	}

        	Console.WriteLine("\n*** REQUIRED PROGRAM***");

        	Console.WriteLine("\nNOTE: Searches are case sensitive");
        	//matching last name
        	string lname = "";
        	
        	Console.Write("\nPlease enter a last name: ");
        	lname = Console.ReadLine();

        	var query = people.Where(x => x.LastName == lname);
        	foreach (var person in query)
        	{
        		Console.WriteLine("Matching criteria: " + person.FirstName + " " + person.LastName + " is a " + person.Occupation + ", and is " + person.Age + " yrs old.");
        	}


        	//matching age and occupation
        	int queryAge = 1;
        	Console.Write("\nPlease enter age: ");
        	while (!int.TryParse(Console.ReadLine(), out queryAge))
        	{
            Console.WriteLine("Age must be numeric.");
        	}

        	string queryOccupation = "";
        	Console.Write("\nPlease enter occupation: ");
        	queryOccupation = Console.ReadLine();

        	List<Person> queryResult = people.Where(x => x.Occupation == queryOccupation && x.Age == queryAge).ToList();
        	
        	foreach (var person in queryResult)
        	{
        		Console.WriteLine("Matching criteria: " + person.FirstName + " " + person.LastName);
        	} 


        	//read key to exit
        	Console.WriteLine("\nPress any key to exit!");
        	Console.ReadKey();

        // ^^^ Write your code above here ^^^
    	
    	}

    	public static List<Person> GenerateListOfPeople()
    	{
        	var people = new List<Person>();

        	people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        	people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        	people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        	people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        	people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });

        	return people;
    	}
	}

	public class Person
	{
    	public string FirstName { get; set; }
    	public string LastName { get; set; }
    	public string Occupation { get; set; }
    	public int Age { get; set; }
	}

	public class FullName
	{
		public string First { get; set; }
		public string Last { get; set; }
	}

}

