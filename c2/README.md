

### LINQ Query Console Application:

 * Create a Console Application using LINQ to query data sets
 * Prompt user for last name. Return full name, occupation, and age
 * Prompt user for age and occupation (Dev or Manager). Return full name. (Includes data validation on numeric data.)
 * Allow user to press any key to return back to command line.

#### Screenshots:

*Screenshots of Program*:

![Program Requirements/Finding Items in Collections](img/p2_1.PNG)

![Finding One Item in Collections](img/p2_2.PNG)

![Finding Data About Collections](img/p2_3.PNG)

![Required Program](img/p2_4.PNG)