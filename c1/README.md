
### Room Calculator:

*Four Parts:*

1. Display Application Name
2. Display Author Name and current Time
3. Perform and display room size calculations, including data validation and rounding to two decimal places
4. Each data member has have get/set methods, also GetArea and GetVolume

#### Screenshots:

*Screenshot of Room Calculator*:

![Room Calculator Requirements](img/room1.PNG)

![Room Calculator Calculations](img/room2.PNG)
