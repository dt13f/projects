

### .Net Web Application:

 * Create an ASP.NET web application
 * Modify the content of the carousel to 3 images of my own
 * Modify the content of each sub section to promote my own work
 * Change the favicon and contact information
 * Have the About page display the current server time

#### Screenshots:

*Screenshot of Home Page*:

![Home Page](img/scrn_1.PNG)

*Screenshot of Contact Page*:

![Contact Page](img/scrn_2.PNG)

*Screenshot of About Page*:

![About Page](img/scrn_3.PNG)
