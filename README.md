# Dylan Thomas - Coding Projects

## *Below are examples of code I have developed in the past*

| C# Applications| Android Applications|
|-------------------|------------------------|
|[Room Calculator README.md](c1/README.md "RoomCalculator README.md file") | [Contact App README.md](a1/README.md "AndroidContact README.md file") |
|[.NET App README.md](net1/README.md "netApp README.md file") | |
|[LINQ Query README.md](c2/README.md "LINQ README.md file") | |
